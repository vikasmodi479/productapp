﻿/*namespace ProductApp.Models
{
    public class UserLoginInfo
    {
        #region Variable
        public string? name;
        public string? password;
        #endregion

        public string? Name { get; set; }
        public string? Password { get; set; }
    }
}*/
namespace ProductApp.Models
{
    public class UserLoginInfo
    {
        //public string? name;
        //public string? password;

        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
