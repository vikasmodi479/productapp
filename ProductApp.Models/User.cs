﻿namespace ProductApp.Models
{
    public class User
    {
        #region Variables
        int? id;
        public string? name;
        public string? password;
        public string? email;
        public string? firstName;
        public string? city;
        #endregion

        #region Class Properties
        public int? Id 
        {
            get { return id; }
            set { id = value; }
        }
        #endregion
        #region Auto Property
        public string Name { get; set; }
         public string Password { get; set; }
         public string Email { get; set; }
        public string FirstName { get; set; }
        public string City { get; set; } = "Bhopal";


        #endregion

        public override string ToString()
        {
            return $"{Id},{Name},{Email},{FirstName},{City}";
        }
    }
}
