﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Exceptions
{
    public class FileNotFoundException:ApplicationException
    {
        public FileNotFoundException()
        {

        }
        public FileNotFoundException(string msg) : base(msg)
        {

        }
    }
}
