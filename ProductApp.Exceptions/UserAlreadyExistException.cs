﻿namespace ProductApp.Exceptions
{
    public class UserAlreadyExistException : ApplicationException
    {
        public UserAlreadyExistException()
        {

        }
        public UserAlreadyExistException(string mas) : base(mas) 
        {

        }
    }
}