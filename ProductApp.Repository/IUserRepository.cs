﻿/*using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    public interface IUserRepository
    {
        /*public bool Register(User user);
        public User[] GetAllUsers();
        public string Login(UserLoginInfo userLoginInfo);

        void getSomeDetails();*/
       // public string Login(UserLoginInfo userLoginInfo);
       // public bool Register(User user);
       // User[] GetAllUsers();
  //  }
//
    using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    public interface IUserRepository
    {
        public bool Login(UserLoginInfo userLoginInfo);
        public bool Register(User user);

        List<User> GetAllUsers();

       // List<string> GetAllUsers();
         User GetUserByName(string username);
       // string GetUserByName(string? userName);
        User GetUserById(int userId);

    }
}
