﻿/*using ProductApp.Models;

namespace ProductApp.Repository
{
    public class UserRepository : UserBookingHistory, IUserRepository
    {

        User[] users;
        public UserRepository()
        {
            users = new User[1];
        }
        public string Login(UserLoginInfo userLoginInfo)
        {
            if ((userLoginInfo.Name == null || userLoginInfo.Name == "") && (userLoginInfo.Password == null || userLoginInfo.Password == ""))
            {
                return "Fill Details";
            }
            else if (users.Length >= 0)
            {
                string loginResult = "";
                foreach (User user in users)
                {
                    if (user.Name == userLoginInfo.Name && user.Password == userLoginInfo.Password)
                    {
                        loginResult = "Login Success";
                    }
                }
                return loginResult;
            }
            else
            {
                return "Invalid Credentials!!!!!!";
            }
        }

        public User[] GetAllUsers()
        {
            return users;
        }

        public bool Register(User user)
        {
            users[0] = user;
            return true;
        }
        public override string getDetails()
        {
            return "details of override method ......";
        }

        // Explicit implementation of Interface method
        void IUserRepository.getSomeDetails()
        {
            Console.WriteLine("Get Some Details Method Called which is explcit implementations");
        }
    }
}*/
using ProductApp.Exceptions;
using ProductApp.Models;

namespace ProductApp.Repository
{
    public class UserRepository : IUserRepository, IFileInfo
    {

        List<User> _users;
       // List<string> _userList;
        public UserRepository()
        {
            _users = new List<User>();
         
        }

        List<User> IUserRepository.GetAllUsers()
        {
            return _users;
        }
        


        bool IUserRepository.Register(User user)
        {
           
            
            string fileName = "userDatabase.txt";
            bool isFileExist = File.Exists(fileName);
            if (!isFileExist)
            {
                File.Create(fileName).Close();
            }
            
                if (isUserNameExits(user.Name, fileName))
                {
                    throw new UserAlreadyExistException($"{user.Name} is already exists");
                    //return false;
                }
                else
                {
                    _users.Add(user);
                    writeContentToFile(user, fileName);
                    return true;
                }
            
            
            
            
            
           
        }

        User IUserRepository.GetUserById(int userId)
        {
            User userObj = new User();
            foreach (var user in _users)
            {
                if (user.Id == userId)
                {
                    userObj = user;
                    break;
                }
            }
            return userObj;
        }

        User IUserRepository.GetUserByName(string username)
        {
            User userobj = new User();
            foreach (var user in _users)
            {
                if (user.Name == username)
                {
                    userobj = user;
                    break;
                }
            }
            return userobj;
        }

        

        bool IUserRepository.Login(UserLoginInfo userLoginInfo)
        {
            bool isUserLoggedIn = false;
            foreach (var user in _users)
            {
                if (user.Name == userLoginInfo.Name && user.Password == userLoginInfo.Password)
                {
                    isUserLoggedIn = true;
                    break;
                }
                else
                {
                    continue;
                }
            }
            return isUserLoggedIn;
        }

        public void writeContentToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))
            {
                sw.WriteLine($"{user}");
                sw.WriteLine();
            }
            
        }

        public void readContentToFile(string fileName)
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                
                string? rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    Console.WriteLine(rowLine);
                }
            }
        }

        public bool isUserNameExits(string? userName, string fileName)
        {
            bool isUserAvailable = false;
           using(StreamReader sr = new StreamReader(fileName))
            {
                string? rowLine;
                while((rowLine = sr.ReadLine()) != null)
                {
                    string[] rowSplittedValue = rowLine.Split(',');    
                    foreach(string value in rowSplittedValue){
                        if(value == userName)
                        {
                            isUserAvailable = true;
                            break;
                        }
                    }
                }
            }
            return isUserAvailable;
        }

        public bool DeleteFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
                return true;
            }
            else
            {
                throw new ProductApp.Exceptions.FileNotFoundException($"{fileName} doesn't exist");
            }
        }
    }
}

