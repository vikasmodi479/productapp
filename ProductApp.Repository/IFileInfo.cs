﻿using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    public interface IFileInfo
    {
        void writeContentToFile(User user, string fileName);
        //List<string> readContentToFile(string fileName);
       public void readContentToFile(string fileName);

        bool isUserNameExits(string? userName, string fileName);
        bool DeleteFile(string fileName);
    }
}
