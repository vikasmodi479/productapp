﻿using ProductApp.Models;
using ProductApp.Repository;
using Xunit;

namespace ProductApp.Tests
{
    public class UserRepositoryTest
    {
        UserRepository _userRepository;
        public UserRepositoryTest()
        {
            _userRepository = new UserRepository();
        }
        [Fact]
        public void TestToRegisterUserReturnTrue()
        {
            //Arrange
            bool expectedRegisterStatus = true;

            //Act
            var actualRegisterSatatus = _userRepository.Register(new User() { Id = 1, Name = "user1", Password = "user1", Email = "user1@gmail.com", FirstName = "user", City = "Delhi" });

            //Assert

            Assert.Equal(expectedRegisterStatus, actualRegisterSatatus);
        }
    }
}